# perf-sut-bootstrap

Gets a QDRIVE3 with ER2 provisioned with basic functionality. At the moment
the defaults are geared for P&S QDRIVE3 SOC1:
```
ansible-playbook  \
-u root \
--ask-pass \
-i 10.16.2.242, \
qdrive3-provision.yml
```

